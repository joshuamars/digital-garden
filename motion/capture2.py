import cv2

Width = 368
Height = 368
threshold = 0.2

PARTS = { "Nose": 0, "Neck": 1, "RightShoulder": 2, "RightElbow": 3, "RightWrist": 4,
         "LeftShoulder": 5, "LeftElbow": 6, "LeftWrist": 7, "RightHip": 8, "RightKnee": 9,
         "RightAnkle": 10, "LeftHip": 11, "LeftKnee": 12, "LeftAnkle": 13, "RightEye": 14,
         "LeftEye": 15, "RightEar": 16, "LeftEar": 17, "Background": 18 }

weights = cv2.dnn.readNetFromTensorflow ("graph_opt.pb")

capture = cv2.VideoCapture (1)
capture.set (cv2.CAP_PROP_FPS, 1)
capture.set (3, 800) # width
capture.set (4, 800) # height
dir (capture)

if not capture.isOpened ():
    capture = cv2.VideoCapture (0)

if not capture.isOpened ():
    raise IOError ("Cannot open camera.")

trackPoints = [(0,0), (0,0), (0,0)]

while cv2.waitKey (1) < 0:
    hasimage, image = capture.read ()
    if not hasimage:
        cv2.waitKey ()
        break


    imageWidth = image.shape[1]
    imageHeight = image.shape[0]

    weights.setInput (cv2.dnn.blobFromImage (image, 1.0, (Width, Height), (127.5, 127.5, 127.5), swapRB = True, crop = False) )
    o = weights.forward ()
    o = o[:, :19, :, :]
    #assert (len (PARTS) == o.shape[1])

    points = []
    for i in range ( len (PARTS) ):
        Map = o[0, i, :, :]
        _, conf, _, point = cv2.minMaxLoc (Map)
        x = (imageWidth * point[0]) / o.shape[3]
        y = (imageHeight * point[1]) / o.shape[2]
        points.append ( (int (x), int (y)) if conf > threshold else None)

    # Draw relevant points

    print (trackPoints )

    # if trackPoints[0] is not None:

        # print (trackPoints[0])
    #if ((trackPoints [0][0] - points[PARTS["Neck"]][0])**2 + (trackPoints [0][1] - points[PARTS["Neck"]][1])**2)**0.5 > 2:

    #cv2.line(image, trackPoints, points[PARTS["Neck"]], (0, 255, 0), 3)

    trackPoints = [points[PARTS["Neck"]], points[PARTS["RightWrist"]], points[PARTS["LeftWrist"]]]

    cv2.ellipse (image, points[PARTS["Neck"]], (3, 3), 0, 0, 360, (0, 0, 255), cv2.FILLED)
    cv2.ellipse (image, points[PARTS["RightWrist"]], (3, 3), 0, 0, 360, (0, 0, 255), cv2.FILLED)
    cv2.ellipse (image, points[PARTS["LeftWrist"]], (3, 3), 0, 0, 360, (0, 0, 255), cv2.FILLED)


    t, _ = weights.getPerfProfile ()
    frequency = cv2.getTickFrequency () / 1000
    cv2.putText (image, '%.2fms' % (t / frequency), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))

    cv2.imshow ('Pose Estimation Using Webcam', image)  