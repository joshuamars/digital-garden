import '@tensorflow/tfjs-node';
import * as poseDetection from '@tensorflow-models/pose-detection';
import * as converter from '@tensorflow/tfjs-converter'; // Added to fix no backend. Doesn't work
import * as tf from '@tensorflow/tfjs-core';
// Register one of the TF.js backends.
import '@tensorflow/tfjs-backend-webgl';

//console.log (tf.getBackend() );

const model = poseDetection.SupportedModels.MoveNet;
const detector = await poseDetection.createDetector(model);//*/

const test = 'test';

export async function getPoses (image) {

	//console.log (image);

	//image = tf.tensor (image.toBytes (), [image.rows, image.cols, -1]);

	//let poses = [0, 0];

	const poses = await detector.estimatePoses(image);

	return poses;

}

//export default getPoses;