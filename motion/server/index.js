var getPoses = null;

( async function () {
  
	getPoses = (await import('posenet')).getPoses;

	console.log (getPoses);

} ) ();

const cv = require ('opencv4nodejs');
const express = require ('express');

const app = express ();
const server = require ('http').Server (app);
const socket_config = {
// ('socket.io')(server, socket_config)
  cors: {
    origin: "http://localhost:8000"
    //origin: "*"
  }
};

const {Image, createCanvas} = require ('canvas');

const io = require ('socket.io')(server, socket_config);

const interval = 30; // 1000 / 30

const camera = new cv.VideoCapture (0);

camera.set (cv.CAP_PROP_FRAME_WIDTH, 500); // Should set to whatever is best for tensorflow...
camera.set (cv.CAP_PROP_FRAME_HEIGHT, 500);

setInterval ( () => {

	const frame = camera.read ();

	// Attempt this: https://github.com/tdurand/tensorflow-node-experiments/blob/master/posenet.js

	let image = new Image ();
	image.src = frame;

	const canvas = createCanvas (image.width, image.height);
	canvas.getContext ('2d').drawImage (image, 0,0);

	if (getPoses) {

		console.log ( getPoses (canvas) );

	}

	//console.log (posenet.getPoses ('test') );

	//io.emit ('message', "Test message for now");

}, interval);

server.listen (3000);