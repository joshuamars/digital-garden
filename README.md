# Digital Garden

Grow digital plants 🌿

## About

This project uses [processing.js](https://github.com/processing/p5.js) to draw digital plants.

## License
This project uses the GNU General Public License, Version 2.0

The main library, p5.js, is licensed with the GNU Lesser General Public License, Version 2.1
A copy of that license is included at [/lib/p5.js/license.txt](/lib/p5.js/license.txt)