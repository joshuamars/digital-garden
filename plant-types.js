/*
 * Digital Garden
 * Copyright (C) 2022  Joshua Mars
 * Email hello@joshuamars.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 */


 // Leaf Type

function leaf_type (name, image, size) {

    this.name = name;
    this.image = image;
    this.size = size;

}


// Stem Type

function stem_type (name, plant_type) {

    this.name = name;
    this.plant = plant_type;
    this.leaf = plant_type.leaf;
    this.length = plant_type.length;
    this.energyCost = 1;
    this.terminal = false; // If terminal, grow a leaf, don't fork. // UNUSED
    this.probability = 0.8; // This stem's probability of growing if define is called // UNUSED
    this.strength = 0.96;
    this.thickness = 1; // UNUSED
    this.thicknessGrower = 1; // Thickness squared, to make parent stem fatter
    this.maxAge = 60*1;// In seconds // UNUSED
    this.sunPull = 1; // Whether it will try to grow towards the sun, and if so how strong is that effect (small effect means little correction at every branch);

    // Branching behavior and properties
    this.forkEvery = 1; // Minimum number of branches before it forks again. // UNUSED
    this.forkEverySpread = 0; // How much fork depth varies randomly // UNUSED
    this.forkAge = 30; // Minimum age before branching (seconds) // UNUSED
    this.forkProbability = 0.8; // Probability that the branch forks when it can. // UNUSED
    this.forkTypes = []; // If this is set (an array of other objects), skip the fork Min and Max??? // UNUSED
    this.forkMin = 2; // Min and max children that could be generated at every fork // UNUSED
    this.forkMax = 3; // UNUSED
    this.forkAngle = 0.5; // 0 is parallel, -1.5 is (almost) perpendicular
    this.forkDepth = 4;

}

const plant_types = {};


// Plant Type

function plant_type (name) {

    this.name = name;
    this.color = 255;
    this.leaf = new leaf_type ( 'Default', leaf_images.default, 10 );
    this.initialDirection = -1.57;
    this.growthSpeed = 0.3;
    this.ageAtMaturity = 10; // UNUSED
    this.ageAtSenescence = 15; // UNUSED
    this.maxAge = 3*60; //

    // Stem properties
	this.maxDepth = 5, // UNUSED
    this.length = 30;
	this.lengthModifier = 1.0;
	this.lengthModifierRange = 0.1;

    // Stems
	this.firstStem = null;
    this.lastStem = null;

}


// Default (Testing) ------------------------------------------------------
plant_types.default =  new plant_type ('Default');

    plant_types.default.firstStem = ( new stem_type ('Main', plant_types.default) ); // Main branch
    plant_types.default.firstStem.forkTypes = new Array ();
    plant_types.default.firstStem.forkTypes.push ( new stem_type ('Main', plant_types.default) );
    plant_types.default.firstStem.forkTypes.push ( new stem_type ('Main', plant_types.default) );
    plant_types.default.firstStem.forkTypes.push ( new stem_type ('Terminal', plant_types.default) );
    plant_types.default.firstStem.forkTypes[2].terminal = true;


// Gambel Oak -------------------------------------------------------------

plant_types.oak = new plant_type ('Gambel Oak');
plant_types.oak.leaf = new leaf_type (plant_types.oak.name, leaf_images.gambel_oak, 20);
plant_types.oak.maxDepth = 10;
plant_types.oak.growthSpeed = 0.5;

var terminalStem = new stem_type ('Terminal', plant_types.oak);

    terminalStem.forkEvery = 1;
    terminalStem.terminal = true;
    terminalStem.forkAngle = 0;
    terminalStem.forkAngleSpread = 1.2;
    terminalStem.sunPull = 1;
    terminalStem.length = 5;
    terminalStem.strength = 0.95;

var clusterStem = new stem_type ('Cluster', plant_types.oak);
    clusterStem.forkEvery = 0;
    clusterStem.forkDepth = 3;
    clusterStem.forkAngle = 0;
    clusterStem.forkAngleSpread = 1.57;
    clusterStem.forkTypes.push ( clusterStem );
    clusterStem.forkTypes.push ( clusterStem );
    clusterStem.forkTypes.push ( terminalStem );

var mainStem = new stem_type ('Main', plant_types.oak);
    mainStem.sunPull = 2;
    mainStem.forkAngle = 0;
    mainStem.forkAngleSpread = 1.57;
    mainStem.forkEvery = 0;
    mainStem.forkTypes.push ( mainStem );
    mainStem.forkTypes.push ( mainStem );
    mainStem.forkTypes.push ( clusterStem );


var cotyledon = new stem_type ('cotyledon', plant_types.oak);
    cotyledon.forkEvery = 0;
    cotyledon.forkAngleSpread = 0;
    cotyledon.forkTypes.push ( mainStem );
    cotyledon.forkTypes.push ( clusterStem );
    cotyledon.forkTypes.push ( clusterStem );

plant_types.oak.firstStem = cotyledon;
plant_types.oak.lastStem = terminalStem;


// Fern -------------------------------------------------------------------

plant_types.fern = new plant_type ('Fern');
plant_types.fern.leaf = new leaf_type (plant_types.fern.name, leaf_images.fern, 16);
plant_types.fern.leaf.sizeFunction = function (branch, size) {
    return ( ( ( branch.maxDepth - branch.depth) / branch.maxDepth ) + 0.5 ) * size;
};

plant_types.fern.maxDepth = 10;
plant_types.fern.length = 5;
plant_types.fern.lengthStep = 0.9;

var terminalStem = new stem_type ('Terminal', plant_types.fern);
    terminalStem.thicknessGrower = 0;
    terminalStem.terminal = true;
    terminalStem.sunPull = 0;
    terminalStem.length = 1;
    terminalStem.forkAngle = 0;
    terminalStem.forkAngleStep = true;
    terminalStem.forkAngleFunction = function (branch) {
        branch.forkAngle = branch.type.forkAngle * ( (branch.maxDepth-branch.depth +2) * 2 / branch.maxDepth );
    }

var terminalStemLeft = Object.create ( terminalStem );
    terminalStemLeft.leaf = new leaf_type (plant_types.fern.name, leaf_images.fern_left, 16);
    terminalStemLeft.leaf.sizeFunction = plant_types.fern.leaf.sizeFunction;
    terminalStemLeft.forkAngle = 1;
    terminalStemLeft.length = 4;

var terminalStemRight = Object.create ( terminalStem );
    terminalStemRight.leaf = new leaf_type (plant_types.fern.name, leaf_images.fern_right, 16);
    terminalStemRight.leaf.sizeFunction = plant_types.fern.leaf.sizeFunction;
    terminalStemRight.forkAngle = -1;
    terminalStemRight.length = 4;

var mainStem = new stem_type ('Main', plant_types.fern);
    mainStem.sunPull = 0;
    mainStem.strength = 0.9;
    mainStem.forkEvery = 0;
    mainStem.thicknessGrower = 0.25;
    mainStem.forkEverySpread = 0;
    mainStem.forkAngle = 0;
    mainStem.forkAngleSpread = 0;
    mainStem.forkDepth = mainStem.plant.maxDepth;
    mainStem.forkTypes.push ( mainStem );
    mainStem.forkTypes.push ( terminalStemLeft );
    mainStem.forkTypes.push ( terminalStemRight );

var cotyledon = new stem_type ('cotyledon', plant_types.fern);
    cotyledon.strength = 0.95;
    cotyledon.forkAngleSpread = 0.3;
    cotyledon.forkEvery = 0;
    cotyledon.length = 40;
    cotyledon.forkTypes.push ( mainStem );

plant_types.fern.firstStem = cotyledon;
plant_types.fern.lastStem = terminalStem;

