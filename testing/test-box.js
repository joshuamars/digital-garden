/*
 * Digital Garden
 * Copyright (C) 2022  Joshua Mars
 * Email hello@joshuamars.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 */

const lines = [];

function mousePressed () {

    mouse.x = mouseX;
    mouse.y = mouseY;

}

function mouseReleased () {

    lines.push ( [mouse.x, mouse.y, mouseX, mouseY] );

    mouse.x = null;
    mouse.y = null;

}

function setup () {

  createCanvas (800, 500);

  frameRate (30);

}

const mouse = { x:null, y:null };

function draw () {

    background (90,140,83);

    if ( mouse.x && mouse.y && mouseIsPressed ) {

        stroke (0,0,0, 128);
        
        point ( mouse.x, mouse.y );

        strokeWeight (3);
        line ( mouse.x, mouse.y, mouseX, mouseY );

    }

    for ( var i=0; i<lines.length; i++ ) {


        // Original line
        stroke (0,0,0, 128);

            strokeWeight (5);
            point ( lines[i][0], lines[i][1] );

            strokeWeight (3);
            line ( lines[i][0], lines[i][1], lines[i][2], lines[i][3] );


        // Reversed line
        stroke (255,255,255, 128);

            /*strokeWeight (5);
            point ( lines[i][1], lines[i][2] );

            strokeWeight (3);
            line ( lines[i][1], lines[i][2], lines[i][3], lines[i][0] );*/


        // Vector
        stroke (0,255,255, 128);

            let midPoint = {
                x: lines[i][0],// + lines[i][2]/2,
                y: lines[i][1]// + lines[i][3]/2,
            }
            point ( midPoint.x, midPoint.y );

            strokeWeight (1);
            let vector = createVector ( lines[i][3] - lines[i][1], lines[i][0] - lines[i][2] );

            line ( midPoint.x, midPoint.y, midPoint.x + vector.x, midPoint.y + vector.y );


        // Manually calculated angle
        stroke (255,255,0, 128);


    // THIS IS WHAT WE'RE AFTER! ------------------------------------------------------

            let angle = atan2 ( lines[i][0] - lines[i][2], lines[i][3] - lines[i][1] );
            if ( angle <= 0 ) { angle -= PI; }

    // --------------------------------------------------------------------------------


            let angleVector = vector.copy ();
            angleVector.setHeading (angle - (PI));

            line ( midPoint.x, midPoint.y, midPoint.x + angleVector.x, midPoint.y + angleVector.y );


        // Finding a simpler solution

        // Must be true: lines[i][0] >= lines[i][2]

        // Must be true: lines[i][2] < lines[i][0]

        if ( angle - 0.2 < vector.heading () < angle + 0.2) {

            stroke (255);
            strokeWeight (2);
            noFill ();

            square (midPoint.x - 30, midPoint.y - 10, 20);

        }

        noStroke ();
        if ( lines[i][0] < lines[i][2] ) { // Red

            fill (255,0,0, 128);
            square (midPoint.x - 30, midPoint.y - 10, 10);

        } else { // Green
            fill (0,255,0, 128);
            square (midPoint.x - 30, midPoint.y - 10, 10);
        }
        if ( lines[i][1] < lines[i][3] ) { // Yellow
            fill (255,255,0, 128);
            square (midPoint.x - 20, midPoint.y - 00, 10);
        } else { // Blue

            fill (0,0,255, 128);
            square (midPoint.x - 20, midPoint.y - 00, 10);
        }


        // Text
        noStroke ();
        fill (0,255,255, 128);

            text ( round ( vector.heading (), 2), midPoint.x + 10, midPoint.y + 4 );
            text ( round ( angle, 2), midPoint.x + 45, midPoint.y + 4 );
            

    }

    

    // Reference angle

    stroke (255,0,0, 128);
    strokeWeight (20);
    point ( 30, height - 30);

    stroke (0);
    strokeWeight (1);

    let referenceVector = createVector (0,12);
    referenceVector.setHeading (0);

    line ( 30, height - 30, 30 + referenceVector.x, height- 30 + referenceVector.y);

    noStroke ();
    fill (255,0,0, 128);
    text (referenceVector.heading (), 60, height - 25 );

}