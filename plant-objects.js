/*
 * Digital Garden
 * Copyright (C) 2022  Joshua Mars
 * Email hello@joshuamars.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 */


// Plant Object Constructors


// Seed constructor -------------------------------------------------------

function seed (x, y, plant_type) {

  this.x = x;
  this.y = y;
  this.type = plant_type;

  this.landed = false;
  this.onLand = function () { this.landed = true; }
  this.age = 0;
  this.velocity = createVector (0, 0);
  this.bounciness = 0.7;
  this.germinantionAge = 0; // Not sure if needed atm?? How long a seed lingers before sprouting.

  this.draw = function () {

    if (isFullscreen) { fill (128); } else { fill (45, 69, 41); }

    noStroke();
    ellipse (this.x, this.y, 5, 5);

    if (this.landed) {

      if (this.age < this.germinationAge) {

        this.age += 1;

      } else { // Germination

        let initialDirection = null;

        if (this.boundary) { // Grow perpendicular to boundary

          initialDirection = atan2 ( this.boundary[0] - this.boundary[2], this.boundary[3] - this.boundary[1] );

          if (initialDirection > 0 ) { initialDirection += PI; }

        }

        growPlant (this.x, this.y, this.type, initialDirection);
        seeds.splice (seeds.indexOf (this), 1); // Self destruct
        delete this;

      }

    } else { fall (this); }

  }

}


// Plant constructor ------------------------------------------------------

function plant (x, y, type=plant_types.default, initialDirection) {

  this.x = x;
  this.y = y;
  this.type = type;
  this.direction = initialDirection || type.initialDirection;
  this.isAlive = true;

  if (plant_colors && id ('use_color_file').checked ) { this.color = color (random (plant_colors)); }
  else { this.color = color (type.color); }
  
  this.plant = this; // Needed so first branch knows that daddy is plant

  this.age = 0;
  this.depth = 1;
  this.maxDepth = type.maxDepth;
  this.branches = new Array ();
  this.children = this.branches;
  this.growthSpeed = type.growthSpeed;

  this.lengthModifier = type.lengthModifier; // This currently gets overwritten in the growPlant function

  this.thicken = function (factor) { return false };

  this.spawnBranch = function (stem_type) {

    if (!this.v) {this.v = createVector (1,1); }

    let newBranch = new branch (this, stem_type);
    newBranch.maxLength = this.lengthModifier * stem_type.length;
    newBranch.v.setHeading (this.direction);

    this.branches.push ( newBranch );

  };

  this.draw = function () { // Draws the plant

    stroke (this.color);
    strokeWeight(5);
    point(this.x, this.y);

    fill (this.color);
    noStroke ();
    text (i, this.x + 4, this.y + 1);

    let branchesAreAlive = false;

    // Draw branches
    for ( var i=0; i<this.branches.length; i++) {

      if (this.branches[i].isAlive) { branchesAreAlive = true; }

      this.branches[i].draw ( this.x, this.y, this.growthSpeed );

    }

    if (this.isAlive && !branchesAreAlive) {

      this.isAlive = false;
      this.branches[0].age = 0;
      this.branches[0].isAttached = false;
      this.branches[0].landed = true;

    }



    this.age ++; // Age plant

    if (this.branches.length == 0) {

      if (plants.length + seeds.length <= maximumPlants) {

        dropRandomSeed (this.type);

      }

      plants.splice (plants.indexOf (this), 1);

      delete this;

    }

  };

} 


// Branch constructor -------------------------------------------------

let branchesCount = 0;

function branch (parent, type) {

  this.id = branchesCount; branchesCount ++;
  this.parent = parent;
  this.plant = parent.plant;
  this.color = parent.color;
  this.age = 0;
  this.maxAge = type.maxAge * frameRate () * random (0.5, 4);
  this.isAlive = true;
  this.isAttached = true;
  this.landed = false;
  this.energy = 60;

  this.type = type;

  this.v = createVector (1, 1); // normal state
  this.bV = {}; // blown vector

  this.depth = parent.depth + 1;
  this.maxDepth = parent.maxDepth;
  if (type.forkAngleFunction) { type.forkAngleFunction (this); }

  let directionModifier = type.forkAngle;
  if (type.forkAngleStep) { directionModifier = 0; }
  else {
    directionModifier += ( type.forkAngleSpread ? random (-type.forkAngleSpread, type.forkAngleSpread) : 0 );
  }
  
  this.v.setHeading ( parent.v.heading () + directionModifier );
  this.v.setHeading ( this.v.copy().add ( sun.copy().mult (type.sunPull) ).heading() );
  this.v.setHeading ( this.v.copy().add ( createVector (0,(1 - type.strength) * this.v.mag () ) ).heading() );
  this.maxLength = type.length ? type.length : parent.maxLength;
  if (this.plant.type.lengthStep) {this.maxLength *= this.plant.type.lengthStep;}
  this.thickness = type.thickness;
  parent.thicken (this.type.thicknessGrower);

  
  this.lastFork = parent.lastFork !== undefined ? parent.lastFork : 0;
  this.forkDepth = type.forkEvery + round (random (-type.forkEverySpread, type.forkEverySpread) );
  this.split = false;
  this.children = new Array ();

  if (type.terminal == true) {

    this.leaves = new Array ();
    this.leaves.push (new leaf (parent.v.x, parent.v.y, parent.v.heading(), this, type.leaf ));

  }

  this.thicken = function (factor) {

    //this.thickness += factor * this.type.growthSpeed; This is making it disappear?
    //this.thickness += factor;
    this.thickness = sqrt (pow (this.thickness, 2) + factor);
    this.parent.thicken (factor);

  };

  this.spawn = function (type) { // type is branch type, not plant type

    let newBranch = new branch (this, type);

    if (type.forkDepth && this.type != type) {newBranch.maxDepth = this.depth + type.forkDepth;}

    this.children.push ( newBranch );

  };

  this.bounciness = 4;

  this.onLand = function () { this.landed = true; this.age = 0; return false; };

  this.die = function () {

    this.isAlive = false;

    for (var i=0;i<this.children.length;i++) {

      this.children[i].die();
    }

    if (this.leaves) {

      for (var i=0;i<this.leaves.length;i++) {

        this.leaves[i].isAlive = false;

      }

    }

  } 

  this.delete = function () {

    for (var i=0;i<this.children.length;i++) {

      this.children[i].delete ();

    }

    if (this.leaves) { 

      this.leaves = null;

    }

    if (this.parent.children) { this.parent.children.splice(this.parent.children.indexOf(this), 1); }

    delete this;

  }

  this.draw = function (originX, originY, growthSpeed, opacity) {

    if (!this.isAttached) {originX = this.x; originY = this.y;}

    this.x = originX; this.y = originY;

    if (this.type.forkAngleStep) {

      let forkAngle = this.forkAngle !== undefined ? this.forkAngle : this.type.forkAngle;

      this.v.setHeading (this.parent.v.heading () + forkAngle * (this.v.mag () / this.type.length ) );

    }

    this.bV = this.v.copy();

    if (!this.landed) {



      // Turbulence

      // Oh god. This is about to be wayyyy too much.

      let turbulenceModifier = createVector(0, 0);

      let branchSize = this.v.mag ();

      for (var i=0; i<turbulence.length; i++ ) {

        // Strength is determined by magnitude of turbulence AND distrance from turbulent point to origin of branch

        /*let strength = turbulence[i].v.mag () * dist (turbulence[i].x, turbulence[i].y, originX, originY) / 50;
        let strengthVector = turbulence[i].v.setMag ( )*/

        let vector = turbulence[i].v.copy();
        vector.setMag ( turbulence[i].v.mag() * this.depth);
        vector.setMag (
          vector.mag() / ( 1 + ( 0.5 * dist (turbulence[i].x, turbulence[i].y, originX, originY) ) )
        );
        vector.mult ( 1.1 - this.type.strength );
        if (!this.plant.branches[0]) { break; } // Temporary fix for bug where branch.draw somehow is being called for a branch that doesn't exist!
        vector.mult ( 1 - (this.thickness / this.plant.branches[0].thickness) );

        turbulenceModifier.add (vector);

      }

      let vector = wind.v.copy();
      vector.setMag ( wind.v.mag() * this.depth / this.maxDepth);
      turbulenceModifier.add (vector);


      // Branch wiggle and such

      this.bV.add ( turbulenceModifier );
      //this.bV.add ( wind.v.copy().setMag ( wind.v.mag () ) );
      this.bV.setHeading ( this.bV.heading () * (0.8 + ( noise(noiseCounter+this.id) * 0.4 ) ) ); // Branch noise wiggle

      if (this.bV.mag() > branchSize * 2 && !this.isAlive) { // Branch is BLOWN OFF!

        this.bV.setMag ( branchSize );
        this.v = this.bV.copy ();
        this.isAttached = false;
      }
      if (this.bV.mag () > branchSize ) { this.bV.setMag ( branchSize ); }

    } else {

      /*if (this.v.x >= 0) { this.bV = createVector (-this.v.mag(), 0); }
      else {this.bV = createVector (this.v.mag(), 0);}*/

    }


    // Draw self

    if (!this.isAttached && !this.landed) {fall (this); }

    if (!this.isAlive && opacity == undefined) {let opacity = 0.5};

    if (this.landed) { opacity = ((litterFade - this.age) / litterFade); }

    strokeColor = color (this.color);
    strokeColor.setAlpha ( (opacity !== undefined ? opacity : 1) * 255);
    stroke (strokeColor);
    strokeWeight (this.thickness);

    line ( originX, originY, originX + this.bV.x, originY + this.bV.y );

    this.age ++; // Age branch
    if (this.age > this.maxAge && this.isAlive) { this.die(); }


    // Grow self

    if (this.isAlive) {

      if (this.v.mag() < this.maxLength) {

        this.v.setMag ( this.v.mag() + growthSpeed );

        

      } else { // If length is greater than max length (branch done growing)

        // Spawn new branches, if there are enough
        if (this.depth < this.maxDepth && !this.split && !this.type.terminal) {

          if (this.lastFork >= this.forkDepth) { // Fork

            this.lastFork = 0;

            for (var i=0;i<this.type.forkTypes.length;i++) {

              this.spawn (this.type.forkTypes[i]);

            }

          } else { // Or ust make another single branch

            this.spawn (this.type.forkTypes[0]);

          }

          this.split = true;

        }

        if (this.depth >= this.maxDepth && ! this.split) {

          // Do something if this is the last branch

          if (this.plant.type.lastStem) { this.spawn (this.plant.type.lastStem); this.split = true;}

        }

      }

    }


    // If landed, fade out

    if (!this.isAttached && this.landed && this.age > litterFade) {

      this.delete ();

    }

    
    // Draw Children

    for (var i=0; i<this.children.length; i++ ) {

      this.children[i].draw ( originX + this.bV.x, originY + this.bV.y, growthSpeed, opacity );

    }

    // Draw leaves

    if (this.leaves) {

      for (var i=0;i<this.leaves.length;i++) {

        this.leaves[i].draw(originX + this.bV.x, originY + this.bV.y, this.bV.heading (), opacity );

      }

    }

  };

}


// Leaf constructor -------------------------------------------------------

function leaf (x, y, direction, parent, type) {

  this.x = x; 
  this.y = y;
  this.direction = 0;
  this.branch = parent;
  this.type = type;

  this.isAlive = true;
  this.isAttached = true; // Subtly distinct. Leaves die and get stuck on.

  this.size = 1;
  this.maxSize = type.size;
  this.image = type.image;

  this.draw = function (x, y, direction, opacity) {

    if (this.isAttached) { // This is to allow the draw function to update the current location of the leaf (since branches move a lot)

      this.x = x; 
      this.y = y;

    }

    push ();
    translate (x, y);
    rotate ( direction + 1.55);

    noStroke ();
    if (opacity) { opacity *= 256; } else { opacity = 256; }
    fill ( this.branch.color, opacity );

    let size = this.size;
    if (this.type.sizeFunction) {size = this.type.sizeFunction (this.branch, this.size);}
    translate (-size / 2, -size);

    this.image (size);
    pop ();


    // Grow

    if (this.size < this.maxSize) {

      this.size += 0.5;

    }

  }

}