let leaf_images = {};

// I used this: https://svg2p5.com/

leaf_images.default = function (size) {

	scale (size / 16);

	beginShape();
	vertex(11.5,3.5);
	bezierVertex(9.5,3.5,9.5,1.5,8,0);
	bezierVertex(7,1,7,4.5,4,4);
	bezierVertex(4,6,1,9,8,16);
	bezierVertex(15.5,9.5,11.5,4.5,11.5,3.5);
	endShape();

};

leaf_images.fern = function (size) {

	scale (size / 32);

	beginShape();
	vertex(16,0);
	bezierVertex(22,10,21,32,16,32);
	bezierVertex(11,32,10,10,16,0);
	endShape();

};

leaf_images.fern_left = function (size) {

	scale (size / 32);

	beginShape();
	vertex(14,0);
	bezierVertex(20,10,21,32,16,32);
	bezierVertex(8,32,14,13,14,0);
	endShape();

};

leaf_images.fern_right = function (size) {

	scale (size / 32);

	beginShape();
	vertex(16,0);
	bezierVertex(12,10,11,32,16,32);
	bezierVertex(24,32,18,13,18,0);
	endShape();

};

leaf_images.gambel_oak = function (size) {

	scale (size / 32);

	beginShape();
	vertex(16,32);
	bezierVertex(21,32,29,27,20,25);
	bezierVertex(29,21,31,17,19,18);
	bezierVertex(31,6,22,8,18,9);
	bezierVertex(23,-2,11,-3,13,7);
	bezierVertex(0,3,9,13,13,17);
	bezierVertex(-4,17,8,24,13,25);
	bezierVertex(4,29,11,32,16,32);
	endShape();

};