/*
 * Digital Garden
 * Copyright (C) 2022  Joshua Mars
 * Email hello@joshuamars.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 */

const mouse = {};

const seedOrBoundaryThreshold = 20;

function mousePressed () {

  draw_behavior = document.querySelector('input[name="draw_behavior"]:checked').value;

  mouse.x = mouseX;
  mouse.y = mouseY;

}

touchStarted = mousePressed;

draw_behavior = 'boundary';

function mouseReleased () {

  if (mouseX < 0 || mouseY < 0 || mouseX > width || mouseY > height) {return;}

  if (mouseButton === LEFT) {

    // Drop a seed
    if ( dist (mouse.x, mouse.y, mouseX, mouseY) < seedOrBoundaryThreshold ) {

      if ( plants.length + seeds.length <= maximumPlants ) {

        let selectedSeedType = document.querySelector('input[name="seed_type"]:checked').value;

        seeds.push (new seed (mouseX, mouseY, plant_types[selectedSeedType]) );

      }
      
    } 

    // Draw either turbulence or a seed
    else {

      if (draw_behavior == 'boundary') {

        boundaries.push ([mouse.x, mouse.y, mouseX, mouseY]);

      }

      else if (draw_behavior == 'turbulence' && !autoMouseTurbulence) {

        // Create turbulence.

        // I want turbulence to be simpler. Just one point, moving
        turbulence.push (new turbulent (mouse.x, mouse.y, createVector (mouseX - mouse.x, mouseY - mouse.y)));

      }

    }

  }

  mouse.x = null;
  mouse.y = null;

}

touchEnded = mouseReleased;

const lastMouse = {};

let autoMouseTurbulence = true;