/*
 * Digital Garden
 * Copyright (C) 2022  Joshua Mars
 * Email hello@joshuamars.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 */

function turbulent (x, y, v) {

  this.x = x;
  this.y = y;
  this.v = v;

  this.draw = function () {

    ellipse (this.x, this.y, 3, 3);
    line (this.x, this.y, 
      this.x + this.v.x,
      this.y + this.v.y,);

    this.v.setMag (this.v.mag() / (1.05 + random (0.1) ) ); // Wiggle

    if (this.v.mag() < 0.1) {

      turbulence.splice(turbulence.indexOf (this), 1); // Self destruct
      delete this;
    }

  }

} // End turbulent definition


const seeds = [];
const plants = [];
const boundaries = [];
const turbulence = [];


// Color file

let plant_colors = null;

load_url ( 'color_file.json', function (json) { plant_colors = JSON.parse (json); } );


function setup () {

  if (isFullscreen) {
    createCanvas (displayWidth, displayHeight);
  } else {
    createCanvas (id ('canvas').scrollWidth || 800, id ('canvas').scrollHeight || 500);
  }

  frameRate (30);

}


function dropRandomSeed (type=plant_types.oak) {

  type = type || plant_types.oak;

  let newSeed = new seed ( random (width-10), random (height-10), type);

  seeds.push ( newSeed );

}


function growPlant (x, y, type=plant_types.default, initialDirection) {

  let newPlant = new plant (x, y, type, initialDirection);

  newPlant.lengthModifier = random (
    type.lengthModifier - type.lengthModifierRange,
    type.lengthModifier + type.lengthModifierRange
  );

  newPlant.spawnBranch (newPlant.type.firstStem);
  
  plants.push (newPlant);

}


let noiseCounter = 0.0;

function draw () {

  for (var i=0; cameraTurbulence && i<cameraTurbulence.length && turbulence.length < 10; i++) {

    turbulence.push ( new turbulent ( 
      cameraTurbulence[i][0],
      cameraTurbulence[i][1],
      createVector ( cameraTurbulence[i][2], cameraTurbulence[i][3] )
    ) );
  }

  noiseCounter += 0.01;

  // Render wind

  wind.v = createVector (wind.speed * wind.direction, 0);
  wind.v.setMag (wind.speed + ( wind.gust * 2 * pow (noise (noiseCounter * (wind.speed / 2)), 2) ) - wind.gust);
  wind.v.setHeading ( (noise (noiseCounter * wind.noiseOffset) * 0.6) - 0.3);

  if ( isFullscreen ) { background (0); } else { background (90,140,83); }

  if ( showFrameRate ) { noStroke (); fill (45,69,41); text ( round ( frameRate (), 2), 10, 20 ); }

  line (width/2, height/2, width/2 + wind.v.x, height/2 + wind.v.y );

  // Create Mouse Turbulence

  if (mouseIsPressed && mouseButton === LEFT && draw_behavior == 'turbulence' && autoMouseTurbulence) {

    mouseTurbulent = new turbulent (lastMouse.x, lastMouse.y, createVector (mouseX - lastMouse.x, mouseY - lastMouse.y));
    //mouseTurbulent.v.mult(2);

    turbulence.push (mouseTurbulent);

    if (turbulence.length > 10 ) { turbulence.shift (); }

  }

  lastMouse.x = mouseX;
  lastMouse.y = mouseY;


  // Draw boundaries

  stroke (45, 69, 41); if (isFullscreen) { stroke (128); }
  strokeWeight (3);

  for ( var i=0; i<boundaries.length; i++ ) {

    line (boundaries[i][0], boundaries[i][1], boundaries[i][2], boundaries[i][3]);

  }

  if (mouseX < 0 || mouseY < 0 || mouseX > width || mouseY > height) { stroke (255,0,128, 64); }

  if (mouseIsPressed && mouseButton === LEFT && draw_behavior == 'boundary' && mouse.x && dist (mouse.x, mouse.y, mouseX, mouseY) > seedOrBoundaryThreshold) {

    line (mouse.x, mouse.y, mouseX, mouseY);

  }


  // If too few plants, make a new one.

  if (plants.length + seeds.length < minimumPlants) { dropRandomSeed (); }

  
  // Draw each plant

  for ( var i=0; i<plants.length; i++ ) {

    plants[i].draw (i);

  }

  // Draw seeds

  for ( var i=0; i<seeds.length; i++ ) {

    seeds[i].draw ();

  }


  // Draw turbulence 

  fill (0, 128, 128);
  stroke (0, 128, 128);
  strokeWeight (1);

  for (var i=0; i<turbulence.length; i++) {

    if ( Array.isArray (turbulence[i].v) ) { turbulence[i].v = createVector ( turbulence[i].v[0], turbulence[i].v[1] ); }

    turbulence[i].draw ();
  }

  if (mouseIsPressed && mouseButton === LEFT && !autoMouseTurbulence && draw_behavior == 'turbulence') {

    ellipse (mouse.x, mouse.y, 3, 3);
    line (mouse.x, mouse.y, mouseX, mouseY);

  }


  // Draw Sun?
  // drawSun ();


  // Draw cursor
  fill (0, 0, 0, 64); if (isFullscreen) { fill (255,255,255, 64); }
  noStroke();
  ellipse (mouseX -1, mouseY -2, 20, 20);


  if (! mouseIsPressed) {noFill();}
  stroke(0, 0, 0, 128);
  strokeWeight(1);
  ellipse (mouseX -1, mouseY -2, 7, 7);

}