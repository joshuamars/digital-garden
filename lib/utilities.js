/*
 * Digital Garden
 * Copyright (C) 2022  Joshua Mars
 * Email hello@joshuamars.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 */


 // My classic id
 function id (x) { return document.getElementById (x); }
 

 // Do two lines intersect?

function intersects (a,b,c,d,p,q,r,s) {
  var det, gamma, lambda;
  det = (c - a) * (s - q) - (r - p) * (d - b);
  if (det === 0) {
    return false;
  } else {
    lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
    gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
    return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
  }
};


// Fall!

function fall (object) {

  if (!object.velocity) { object.velocity = createVector (0, 0); }

  object.velocity.y += gravity;

      
  // If it WILL hit a boundary

  for (var i=0; i<boundaries.length; i++ ) {

    if (intersects (
      object.x, object.y,
      object.x + object.velocity.x, object.y + object.velocity.y,
      boundaries[i][0], boundaries[i][1],
      boundaries[i][2], boundaries[i][3]
    )) {

      // BOUNCE!! Reflect velocity off of boundary wall

      /*this.velocity = this.velocity.div(bounciness).cross (createVector(
        boundaries[i][2] - boundaries[i][0],
        boundaries[i][3] - boundaries[i][1]
      ));*/

      object.velocity.y = -object.velocity.y / (1 + object.bounciness); // Bounce

      if (object.velocity.mag () < 3) { 

        object.onLand (); 
        object.boundary = boundaries[i];

      }

    }

  }

  object.x += object.velocity.x;
  object.y += object.velocity.y;


  // If it HAS hit the bottom

  if (object.y >= height) { // Hit bottom?

    object.velocity.y = -object.velocity.y / (1 + object.bounciness); // Bounce

    object.y = height; // Don't clip through the ground??

    if (object.velocity.mag () < 3) { object.onLand(); }

  }

}


// Get something

function load_url ( url, on_load_function ) {
    
  let request = new XMLHttpRequest();
  request.open ( 'GET', url );
  request.responseType = 'text';
  request.onload = function () { on_load_function ( request.response ); };
  request.send ();
  
}
