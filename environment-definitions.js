/*
 * Digital Garden
 * Copyright (C) 2022  Joshua Mars
 * Email hello@joshuamars.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 */


// Gravity - how far a seed will accelerate per frame, in pixels

const gravity = 1;

// Light source, in radians. Because the sun is normally super far, this works fine.
// I could change this to be be coordinates to similate stuff light grow lights


// Sun

//const lightSource = -1.5; // Half pi away from 0 is straight up
const sun = new p5.Vector (0, -1); // Straight up

function drawSun () { // Temporary
  fill (255,255,0,128);
  noStroke ();
  ellipse (width/2,height/2, 30, 30);
  stroke (0,0,0);
  strokeWeight (1);
  line (width/2,height/2, width/2 + sun.copy().mult(15).x, height/2 + sun.copy().mult(15).y);
}


// Wind - what direction and speed is the wind blowing?

// https://explore.synopticdata.com/NHMU/current

// https://api.synopticdata.com/v2/stations/latest?&stid=NHMU&units=metric,speed|kph,pres|mb&showemptystations=1&token=509725438f644537afac5e1ca7984e8b

var wind = { speed: 5, direction: -1, gust: 1, noiseOffset: 50, noiseSpread:  0.3 };

let latest_weather_data;

load_url ( 'test_weather_data.json', function (json) {

    wind = {};

    try { latest_weather_data = JSON.parse (json); } catch (error) { console.error (error); }

    if (!latest_weather_data || !latest_weather_data.STATION[0] || !latest_weather_data.STATION[0].OBSERVATIONS) {

        return false;

    }

    wind.speed = latest_weather_data.STATION[0].OBSERVATIONS.wind_speed_value_1.value / 10;

    wind.direction = latest_weather_data.STATION[0].OBSERVATIONS.wind_direction_value_1.value;
    wind.direction = (wind.direction < 180) ? -1 : 1; // If East, blow to the left (flipped cause wall projection is on south wall)

    wind.gust = latest_weather_data.STATION[0].OBSERVATIONS.wind_gust_value_1.value / 10;
    wind.gust = wind.gust - wind.speed;

} );


isFullscreen = false;

showFrameRate = false;


minimumPlants = 1;

maximumPlants = 10;


litterFade = 300; // How long dropped branches hang around for.